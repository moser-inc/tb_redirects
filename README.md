# TB Redirects

TB Redirects is an engine for Twice Baked that provides basic redirection capabilities. This engine is intended for those already familiar with tb_core.

## Requirements

We require Rails 5, tb_core 1.4, and ruby 2.3.1 or higher.

## Installation

Add the gem to your Gemfile.

    gem 'tb_redirects'

Install the gem and run the migrations.

    bundle install
    rake railties:install:migrations
    rake db:migrate

Then restart your application.

## How it Works

In general, Twice Baked will raise a `TbCore::NotFoundError` error any time a 404 happens. Under normal circumstances the core engine will rescue from that error and display a 404 page. TB Redirects works by rescuing from that error before core is able to do so and checking the redirects table for applicable paths.

What this means for your controllers is that, in order for redirection to work propertly, you should always be raising `TbCore::NotFoundError` when your intention is to render a 404 page. This will allow tb_core and tb_redirects to do their thing.

## Create a Redirect

You can create a redirect in one of two ways. The first and easiest way is to create it through the Redirects admin module. Simply enter a `source` and `destination` and save it.

The second way is through code. You can create a redirect using the `create_smart` method.

    TbRedirect.create_smart({
      :source => '/my/path',
      :destination => '/my/result'
      :created_by => 'system'
    })

- The `create_smart` method will automatically check for redirects with the same `:source` value, and if it finds one, that record will be updated instead of creating a new one.
- The `created_by` attribute can be any string - use this to keep track of where the redirects are coming from.
