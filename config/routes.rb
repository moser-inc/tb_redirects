Rails.application.routes.draw do
  namespace :admin do
    resources :tb_redirects, :path => 'redirects'
  end
end
