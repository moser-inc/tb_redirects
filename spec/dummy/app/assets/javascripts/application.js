//= require jquery
//= require tb_core
//= require bootstrap_modules
//= require_self
//= require_directory .

(function(){

window.app = {
  init: function(){
    // global initializer
  }
};

$(document).ready(app.init);

})();
