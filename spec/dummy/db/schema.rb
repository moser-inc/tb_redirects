# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_160_212_211_331) do
  create_table 'spud_permissions', force: :cascade do |t|
    t.string   'name',       limit: 255, null: false
    t.string   'tag',        limit: 255, null: false
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  add_index 'spud_permissions', ['tag'], name: 'index_spud_permissions_on_tag', unique: true, length: { 'tag' => 191 }, using: :btree

  create_table 'spud_role_permissions', force: :cascade do |t|
    t.integer  'spud_role_id',        limit: 4,   null: false
    t.string   'spud_permission_tag', limit: 255, null: false
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  add_index 'spud_role_permissions', ['spud_permission_tag'], name: 'index_spud_role_permissions_on_spud_permission_tag', length: { 'spud_permission_tag' => 191 }, using: :btree
  add_index 'spud_role_permissions', ['spud_role_id'], name: 'index_spud_role_permissions_on_spud_role_id', using: :btree

  create_table 'spud_roles', force: :cascade do |t|
    t.string   'name',       limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  create_table 'spud_user_settings', force: :cascade do |t|
    t.integer  'spud_user_id', limit: 4
    t.string   'key',          limit: 255
    t.string   'value',        limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
  end

  create_table 'spud_users', force: :cascade do |t|
    t.string   'first_name',               limit: 255
    t.string   'last_name',                limit: 255
    t.boolean  'super_admin'
    t.string   'login',                    limit: 255,                 null: false
    t.string   'email',                    limit: 255,                 null: false
    t.string   'crypted_password',         limit: 255,                 null: false
    t.string   'password_salt',            limit: 255,                 null: false
    t.string   'persistence_token',        limit: 255,                 null: false
    t.string   'single_access_token',      limit: 255,                 null: false
    t.string   'perishable_token',         limit: 255,                 null: false
    t.integer  'login_count',              limit: 4,   default: 0,     null: false
    t.integer  'failed_login_count',       limit: 4,   default: 0,     null: false
    t.datetime 'last_request_at'
    t.datetime 'current_login_at'
    t.datetime 'last_login_at'
    t.string   'current_login_ip',         limit: 255
    t.string   'last_login_ip',            limit: 255
    t.datetime 'created_at'
    t.datetime 'updated_at'
    t.string   'time_zone',                limit: 255
    t.integer  'spud_role_id',             limit: 4
    t.boolean  'requires_password_change', default: false
  end

  add_index 'spud_users', ['email'], name: 'index_spud_users_on_email', length: { 'email' => 191 }, using: :btree
  add_index 'spud_users', ['login'], name: 'index_spud_users_on_login', length: { 'login' => 191 }, using: :btree
  add_index 'spud_users', ['spud_role_id'], name: 'index_spud_users_on_spud_role_id', using: :btree

  create_table 'tb_redirects', force: :cascade do |t|
    t.string   'owner_type',  limit: 255
    t.integer  'owner_id',    limit: 4
    t.string   'source',      limit: 255, null: false
    t.string   'destination', limit: 255, null: false
    t.string   'created_by',  limit: 255
    t.datetime 'created_at',              null: false
    t.datetime 'updated_at',              null: false
  end

  add_index 'tb_redirects', ['owner_type', 'owner_id'], name: 'index_tb_redirects_on_owner_type_and_owner_id', length: { 'owner_type' => 191, 'owner_id' => nil }, using: :btree
  add_index 'tb_redirects', ['source'], name: 'index_tb_redirects_on_source', unique: true, length: { 'source' => 191 }, using: :btree

  create_table 'widgets', force: :cascade do |t|
    t.string   'name', limit: 255
    t.datetime 'created_at',             null: false
    t.datetime 'updated_at',             null: false
  end
end
