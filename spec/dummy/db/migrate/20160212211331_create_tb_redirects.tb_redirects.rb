# This migration comes from tb_redirects (originally 20160211162513)
class CreateTbRedirects < ActiveRecord::Migration[4.2]
  def change
    create_table :tb_redirects do |t|
      t.string :owner_type
      t.integer :owner_id
      t.string :source, null: false
      t.string :destination, null: false
      t.string :created_by
      t.timestamps null: false
    end
    add_index :tb_redirects, [:owner_type, :owner_id]
    add_index :tb_redirects, :source, unique: true
  end
end
