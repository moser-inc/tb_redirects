require 'rails_helper'

RSpec.describe TbRedirects::HasRedirects do
  describe 'included' do
    it 'should add a has_many relationship' do
      widget = Widget.create(name: 'Test')
      redirect = widget.tb_redirects.create(source: 'a', destination: 'b')
      expect(widget.tb_redirects).to eq([redirect])
    end

    it 'should delete the related redirects' do
      widget = Widget.create(name: 'Test')
      redirect = widget.tb_redirects.create(source: 'a', destination: 'b')
      widget.destroy
      expect do
        TbRedirect.find(redirect.id)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
