FactoryBot.define do
  factory :tb_redirect do
    owner_type nil
    owner_id nil
    sequence(:source) { |n| "/test-#{n}" }
    sequence(:destination) { |n| "/redirected-#{n}" }
    created_by 'user'
  end
end
