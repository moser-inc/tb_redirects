require 'rails_helper'

RSpec.describe Admin::TbRedirectsController, type: :controller do
  before(:each) do
    activate_session(admin: true)
  end

  describe 'index' do
    it 'should render the index' do
      get :index
      expect(response).to have_http_status :success
      expect(response).to render_template :index
    end
  end

  describe 'show' do
    it 'should render the show page' do
      tb_redirect = FactoryBot.create(:tb_redirect)
      get :show, params: { id: tb_redirect.id }
      expect(response).to have_http_status :success
      expect(response).to render_template :show
    end

    it 'should return a 404 error' do
      get :show, params: { id: 1 }
      expect(response).to have_http_status :not_found
    end
  end

  describe 'new' do
    it 'should render the new page' do
      get :new
      expect(response).to have_http_status :success
      expect(response).to render_template :new
    end
  end

  describe 'edit' do
    it 'should render the edit page' do
      tb_redirect = FactoryBot.create(:tb_redirect)
      get :edit, params: { id: tb_redirect.id }
      expect(response).to have_http_status :success
      expect(response).to render_template :edit
    end
  end

  describe 'create' do
    it 'should create the record' do
      expect do
        post :create, params: { tb_redirect: FactoryBot.attributes_for(:tb_redirect) }
      end.to change(TbRedirect, :count).by(1)
    end
  end

  describe 'update' do
    it 'should update the record' do
      tb_redirect = FactoryBot.create(:tb_redirect)
      current_value = tb_redirect.source
      new_value = current_value + '-Updated'
      expect do
        patch :update, params: { id: tb_redirect.id, tb_redirect: { source: new_value } }
        tb_redirect.reload
      end.to change(tb_redirect, :source).to(new_value)
    end
  end

  describe 'destroy' do
    it 'should destroy the record' do
      tb_redirect = FactoryBot.create(:tb_redirect)
      expect do
        delete :destroy, params: { id: tb_redirect.id }
      end.to change(TbRedirect, :count).by(-1)
    end
  end
end
