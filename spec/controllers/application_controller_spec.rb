require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  controller do
    def index
      raise TbCore::NotFoundError
    end
  end

  describe '#check_for_applicable_redirect' do
    it 'should redirect' do
      FactoryBot.create(:tb_redirect, source: '/anonymous', destination: '/redirected')
      get :index
      expect(response).to redirect_to('/redirected')
    end

    it 'should not redirect' do
      FactoryBot.create(:tb_redirect, source: '/other', destination: '/redirected')
      get :index
      expect(response).to_not redirect_to('/redirected')
    end
  end
end
