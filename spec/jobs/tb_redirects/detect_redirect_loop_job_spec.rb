require 'rails_helper'

RSpec.describe TbRedirects::DetectRedirectLoopJob, type: :job do
  before do
    TbRedirect.skip_callback(:save, :after, :schedule_loop_detection)
  end
  after do
    TbRedirect.set_callback(:save, :after, :schedule_loop_detection)
  end

  describe '#perform' do
    it 'should destroy a redirect' do
      redirects = TbRedirect.create([
                                      { source: '/1', destination: '/2', created_by: 'user' },
                                      { source: '/2', destination: '/3', created_by: 'user' },
                                      { source: '/3', destination: '/1', created_by: 'user' }
                                    ])
      expect do
        TbRedirects::DetectRedirectLoopJob.perform_now(redirects.first)
      end.to change(TbRedirect, :count).by(-1)
    end

    it 'should not destroy anything' do
      redirects = TbRedirect.create([
                                      { source: '/1', destination: '/2', created_by: 'user' },
                                      { source: '/2', destination: '/3', created_by: 'user' },
                                      { source: '/3', destination: '/4', created_by: 'user' }
                                    ])
      expect do
        TbRedirects::DetectRedirectLoopJob.perform_now(redirects.first)
      end.to_not change(TbRedirect, :count)
    end
  end

  describe '#search_parent' do
    before(:each) do
      @redirects = TbRedirect.create([
                                       { source: '/1', destination: '/2', created_by: 'user' },
                                       { source: '/2', destination: '/3', created_by: 'user' },
                                       { source: '/3', destination: '/1', created_by: 'user' }
                                     ])
      @redirect = @redirects.first
    end

    it 'should destroy a redirect' do
      job = TbRedirects::DetectRedirectLoopJob.new(@redirect)
      expect do
        job.send(:search_parent, @redirects.second, @redirect.destination)
      end.to change(TbRedirect, :count).by(-1)
    end

    it 'should continue searching' do
      job = TbRedirects::DetectRedirectLoopJob.new(@redirect)
      expect do
        job.send(:search_parent, @redirects.third, @redirect.destination)
      end.to change(job, :attempts).by(2)
    end

    it 'should give up after MAX_ATTEMPTS' do
      job = TbRedirects::DetectRedirectLoopJob.new(@redirect)
      job.instance_variable_set(:@attempts, TbRedirects::DetectRedirectLoopJob::MAX_ATTEMPTS + 1)
      expect do
        job.send(:search_parent, @redirects.second, @redirect.destination)
      end.to raise_error(TbRedirects::DetectRedirectLoopJob::AttemptsExceeded)
    end
  end
end
