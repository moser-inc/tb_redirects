require 'rails_helper'

RSpec.describe TbRedirect, type: :model do
  describe '.find_with_uri' do
    before(:each) do
      @sample_a = FactoryBot.create(:tb_redirect, source: '/test', destination: '/redirected')
      @sample_b = FactoryBot.create(:tb_redirect, source: 'http://test.com/blog', destination: '/redirected')
      @sample_c = FactoryBot.create(:tb_redirect, source: 'http://test.com/about', destination: '/redirected')
    end

    it 'should find a record where the path matches' do
      result = TbRedirect.find_with_uri('http://google.com/test')
      expect(result).to eq(@sample_a)
    end

    it 'should find a record where the full URI matches' do
      result = TbRedirect.find_with_uri('http://test.com/blog')
      expect(result).to eq(@sample_b)
    end

    it 'should find no records' do
      result = TbRedirect.find_with_uri('http://test.com/contact')
      expect(result).to eq(nil)
    end

    it 'should catch an invalid URI error' do
      expect do
        TbRedirect.find_with_uri('%!&*')
      end.to_not raise_error
    end
  end

  describe '.create_smart' do
    it 'should create a new redirect' do
      redirect_1 = FactoryBot.create(:tb_redirect)
      expect do
        redirect_2 = TbRedirect.create_smart(FactoryBot.attributes_for(:tb_redirect, source: '/new'))
      end.to change(TbRedirect, :count).by(1)
    end

    it 'should update an existing redirect' do
      redirect_1 = FactoryBot.create(:tb_redirect)
      expect do
        redirect_2 = TbRedirect.create_smart(FactoryBot.attributes_for(:tb_redirect, source: redirect_1.source))
      end.to_not change(TbRedirect, :count)
    end
  end

  describe '#source_not_equals_destination' do
    it 'should not allow the source and destination to be equal' do
      redirect_1 = FactoryBot.build(:tb_redirect, source: 'a', destination: 'a')
      expect(redirect_1).to be_invalid
    end
  end

  describe '#destroy_opposite_redirect' do
    it 'should destroy the other redirect' do
      redirect_1 = FactoryBot.build(:tb_redirect, source: 'a', destination: 'b')
      redirect_2 = FactoryBot.build(:tb_redirect, source: 'b', destination: 'a')
      expect do
        redirect_1.reload
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '#schedule_loop_detection' do
    it 'should schedule a job' do
      parent = FactoryBot.create(:tb_redirect)
      expect do
        FactoryBot.create(:tb_redirect, source: parent.destination)
      end.to have_enqueued_job(TbRedirects::DetectRedirectLoopJob)
    end
  end
end
