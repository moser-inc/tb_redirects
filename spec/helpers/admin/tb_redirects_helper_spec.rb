require 'rails_helper'

RSpec.describe Admin::TbRedirectsHelper, type: :helper do
  describe '#created_by_for_tb_redirect' do
    it 'should return an info label' do
      result = helper.created_by_for_tb_redirect('user')
      expect(result).to include('label-info')
    end
    it 'should return a default label' do
      result = helper.created_by_for_tb_redirect('cms')
      expect(result).to include('label-default')
    end
  end
end
