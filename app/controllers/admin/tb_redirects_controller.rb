class Admin::TbRedirectsController < Admin::ApplicationController

  belongs_to_app :tb_redirects
  add_breadcrumb 'Redirects', :admin_tb_redirects_path
  before_action :load_tb_redirect, only: [:show, :edit, :update, :destroy]

  def index
    @tb_redirects = TbRedirect.ordered.paginate(page: params[:page])
    @tb_redirects = @tb_redirects.search(params[:search]) if params[:search]
    respond_with @tb_redirects
  end

  def show
    respond_with @tb_redirect
  end

  def new
    @tb_redirect = TbRedirect.new
    respond_with @tb_redirect
  end

  def create
    @tb_redirect = TbRedirect.create_smart(tb_redirect_params.merge(created_by: 'user'))
    flash[:notice] = 'Redirect created successfully' if @tb_redirect.errors.none?
    respond_with @tb_redirect, location: admin_tb_redirects_path
  end

  def edit
    respond_with @tb_redirect
  end

  def update
    if @tb_redirect.update_attributes(tb_redirect_params)
      flash[:notice] = 'Redirect updated successfully'
    end
    respond_with @tb_redirect, location: admin_tb_redirects_path
  end

  def destroy
    flash[:notice] = 'Redirect deleted successfully' if @tb_redirect.destroy
    respond_with @tb_redirect, location: admin_tb_redirects_path
  end

  private

  def load_tb_redirect
    @tb_redirect = TbRedirect.find_by!(id: params[:id])
  end

  def tb_redirect_params
    params.require(:tb_redirect).permit(:source, :destination)
  end

end
