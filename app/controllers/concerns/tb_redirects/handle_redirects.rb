module TbRedirects::HandleRedirects
  extend ActiveSupport::Concern

  included do
    rescue_from TbCore::NotFoundError, with: :check_for_applicable_redirect
  end

  def check_for_applicable_redirect(error)
    if redirect = TbRedirect.find_with_uri(request.original_url)
      redirect_to redirect.destination, status: :moved_permanently
    else
      handle_request_error(error)
    end
  end
end
