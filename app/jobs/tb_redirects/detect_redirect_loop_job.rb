class TbRedirects::DetectRedirectLoopJob < ActiveJob::Base
  queue_as :default

  attr_reader :attempts
  MAX_ATTEMPTS = 100

  def initialize(*arguments)
    super(*arguments)
    @attempts = 0
  end

  def perform(redirect)
    parents = TbRedirect.where(destination: redirect.source)
    parents.each { |p| search_parent(p, redirect.destination) }
  end

  private

  class AttemptsExceeded < StandardError
  end

  def search_parent(parent, destination_to_check)
    @attempts += 1
    raise AttemptsExceeded, "Exceeded #{MAX_ATTEMPTS} max attempts" if @attempts > MAX_ATTEMPTS

    if parent.source == destination_to_check
      parent.destroy
    else
      parents = TbRedirect.where(destination: parent.source)
      parents.each { |p| search_parent(p, destination_to_check) }
    end
  end

end
