class TbRedirect < ActiveRecord::Base
  scope :ordered, -> { order('source desc') }
  scope :search, ->(term) { where('source LIKE ?', "%#{term}%") }

  belongs_to :owner,
    polymorphic: true,
    inverse_of: :tb_redirects,
    optional: true

  validates :source, :destination, :created_by, presence: true
  validates :source, uniqueness: true

  validate :source_not_equals_destination
  after_save :destroy_opposite_redirect
  after_save :schedule_loop_detection

  def self.find_with_uri(uri)
    path = URI(uri).path
    uri_without_params = uri.split('?').first
    find_by(
      arel_table[:source].eq(path)
      .or(arel_table[:source].eq(uri_without_params))
    )
  rescue URI::InvalidURIError => e
    logger.debug e.message
    return nil
  end

  def self.create_smart(params)
    redirect = find_or_initialize_by(source: params.delete(:source))
    redirect.assign_attributes(params)
    redirect.save()
    return redirect
  end

  private

  def source_not_equals_destination
    if source == destination
      errors.add(:destination, 'cannot be the same as the source')
    end
    return true
  end

  def destroy_opposite_redirect
    opposite = TbRedirect.find_by(source: destination,
                                  destination: source)
    opposite.destroy if opposite.present?
    return true
  end

  def schedule_loop_detection
    if TbRedirect.where(destination: source).count > 0
      TbRedirects::DetectRedirectLoopJob.perform_later(self)
    end
    return true
  end

end
