module TbRedirects::HasRedirects
  extend ActiveSupport::Concern

  included do
    has_many :tb_redirects, dependent: :destroy, as: :owner
  end
end
