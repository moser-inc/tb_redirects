module Admin::TbRedirectsHelper
  def created_by_for_tb_redirect(created_by)
    if created_by == 'user'
      content_tag :span, created_by.titleize, class: 'label label-info'
    else
      content_tag :span, created_by.titleize, class: 'label label-default'
    end
  end
end
