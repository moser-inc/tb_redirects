require 'tb_core'

module TbRedirects
  class Engine < ::Rails::Engine
    engine_name 'tb_redirects'
    config.autoload_paths += Dir["#{config.root}/lib/**/"]

    config.generators do |g|
      g.test_framework :rspec, fixture: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.assets false
      g.helper true
    end

    initializer 'tb_redirects.admin', before: 'tb_core.admin' do |_config|
      TbCore.admin_applications += [
        { name: 'Redirects', key: :tb_redirects, thumbnail: 'admin/module_icon.png', url: '/admin/redirects' }
      ]
    end

    initializer 'tb_redirects.controllers' do |_config|
      ActiveSupport.on_load(:action_controller) do
        TbCore::ApplicationController.send :include, TbRedirects::HandleRedirects
      end
    end

  end
end
