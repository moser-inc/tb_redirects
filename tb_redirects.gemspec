$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'tb_redirects/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'tb_redirects'
  s.version     = TbRedirects::VERSION
  s.authors     = ['Greg Woods']
  s.email       = ['greg@westlakedesign.com']
  s.homepage    = 'https://bitbucket.org/westlakedesign/tb_redirects'
  s.summary     = 'Simple redirect management for Twice Baked'
  s.description = 'TbRedirects provides a tool for creating and managing 301 redirects for your website'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir.glob('spec/**/*').reject { |f| f.match(/^spec\/dummy\/(log|tmp)/) }

  s.add_dependency 'tb_core', '>= 1.4.4'
  s.add_dependency 'rails', '>= 5.0.0.1'

  s.add_development_dependency 'mysql2'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rails-controller-testing'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'rubocop'
end
